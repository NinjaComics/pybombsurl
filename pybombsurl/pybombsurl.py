#!/usr/bin/env python

import subprocess
import string
from pybombs import recipe
from urllib.parse import urlparse

MAX_URL_LEN = 255

# whitelist for the uri
whitelist = []
whitelist.extend(string.ascii_letters)
whitelist.extend(string.digits)
whitelist.extend(['_',':','?','/','+','.','~','=','<','>','-',',','$','&',' '])

class InvalidUrlException(Exception):
    def __init__(self, url, msg=""):
        self.url = url
        self.message = msg
    def __str__(self):
        return self.message

class PybombsUrl(object):
    def __init__(self, url):
        self.url = url
        self.packages = {}

    def url_parser(self, pybombs_url):
        parser = urlparse(pybombs_url)
        if len(pybombs_url) > MAX_URL_LEN:
            raise InvalidUrlException(
                pybombs_url, "Url string {} too long".format(pybombs_url))

        for char in pybombs_url:
            if not char in whitelist:
                raise InvalidUrlException(pybombs_url,
                                          "Non whitelist char in the url")
            else:
                continue

        if not 'pybombs' in parser.scheme:
            notif = 'Invalid PyBOMBS URL'
            proc_output(notif)
        else:
            app_list = parser.path.split(",")
        return app_list

    def pkg_deps(self):
        pkgs = self.url_parser(self.url)
        for pkg in pkgs:
            try:
                pkg_recipe = recipe.get_recipe(pkg, target='package')
                deps = pkg_recipe.depends
                deps = ', '.join(deps)
                self.packages[pkg] = deps
            except Exception as exp:
                notify_error(exp)
        return self.packages

    def desktop_notify(self, notif_msg, status=False):
        if status == True:
            notification = ['notify-send', 'PybombsUrl', notif_msg,
                            '--icon=dialog-information']
        else:
            notification = ['notify-send', 'PybombsUrl', notif_msg,
                            '--icon=dialog-error']
        return notification

    def proc_output(self, cmd):
        proc = subprocess.Popen(cmd, shell=False)
        return proc
