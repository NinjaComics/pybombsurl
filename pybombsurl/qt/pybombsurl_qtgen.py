# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'pybombsurl/qt/pybombsurl-qt.ui'
#
# Created by: PyQt5 UI code generator 5.7
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_PybombsUrlDialog(object):
    def setupUi(self, PybombsUrlDialog):
        PybombsUrlDialog.setObjectName("PybombsUrlDialog")
        PybombsUrlDialog.resize(480, 320)
        self.textEdit = QtWidgets.QTextEdit(PybombsUrlDialog)
        self.textEdit.setGeometry(QtCore.QRect(0, 110, 501, 141))
        self.textEdit.setReadOnly(True)
        self.textEdit.setObjectName("textEdit")
        self.widget = QtWidgets.QWidget(PybombsUrlDialog)
        self.widget.setGeometry(QtCore.QRect(0, 110, 501, 141))
        self.widget.setObjectName("widget")
        self.progressBar = QtWidgets.QProgressBar(self.widget)
        self.progressBar.setGeometry(QtCore.QRect(15, 80, 455, 23))
        self.progressBar.setProperty("value", 24)
        self.progressBar.setObjectName("progressBar")
        self.label_2 = QtWidgets.QLabel(self.widget)
        self.label_2.setGeometry(QtCore.QRect(10, 20, 471, 31))
        self.label_2.setText("")
        self.label_2.setObjectName("label_2")
        self.label = QtWidgets.QLabel(PybombsUrlDialog)
        self.label.setGeometry(QtCore.QRect(-8, -10, 500, 125))
        self.label.setText("")
        self.label.setPixmap(QtGui.QPixmap(":/noprefix/assets/dialog_banner.png"))
        self.label.setObjectName("label")
        self.pushButton = QtWidgets.QPushButton(PybombsUrlDialog)
        self.pushButton.setGeometry(QtCore.QRect(380, 265, 101, 41))
        self.pushButton.setObjectName("pushButton")
        self.pushButton_2 = QtWidgets.QPushButton(PybombsUrlDialog)
        self.pushButton_2.setGeometry(QtCore.QRect(265, 265, 101, 41))
        self.pushButton_2.setDefault(True)
        self.pushButton_2.setObjectName("pushButton_2")
        self.pushButton_3 = QtWidgets.QPushButton(PybombsUrlDialog)
        self.pushButton_3.setGeometry(QtCore.QRect(380, 265, 101, 41))
        self.pushButton_3.setObjectName("pushButton_3")

        self.retranslateUi(PybombsUrlDialog)
        QtCore.QMetaObject.connectSlotsByName(PybombsUrlDialog)

    def retranslateUi(self, PybombsUrlDialog):
        _translate = QtCore.QCoreApplication.translate
        PybombsUrlDialog.setWindowTitle(_translate("PybombsUrlDialog", "PyBOMBS App Store"))
        self.pushButton.setText(_translate("PybombsUrlDialog", "Cancel"))
        self.pushButton_2.setText(_translate("PybombsUrlDialog", "Apply"))
        self.pushButton_3.setText(_translate("PybombsUrlDialog", "Cancel"))

import pybombsurlicons_rc
