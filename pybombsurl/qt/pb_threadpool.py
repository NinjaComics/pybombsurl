#Pybombs imports
from pybombs import install_manager, pb_logging

#PyQt imports
from PyQt5 import QtCore

#A worker thread that takes care of install/update/remove tasks
class AWorkerThread(QtCore.QThread):
    progress_tick = QtCore.pyqtSignal(int, int, str)
    info_tick = QtCore.pyqtSignal(str,str)

    def __init__(self, package_list):
        QtCore.QThread.__init__(self)
        self.package_list = package_list

    def run(self):
        instaman = install_manager.InstallManager()

        for package in self.package_list:
            pkg_index = self.package_list.index(package)+1
            self.progress_tick.emit(pkg_index, len(self.package_list), 'install')
            if instaman.install([package], 'install'):
                self.info_tick.emit("PybombsUrl Update", "Installed {} successfully".format(package))
            else:
                self.info_tick.emit('PybombsUrl Error',
                                     "Failed to install {}".format(package))
        return
