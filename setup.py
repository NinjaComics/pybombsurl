#!/usr/bin/env python3

import setuptools
from setuptools import setup

deps = ["pybombs",]

setup(name='PybombsUrl',
      version='0.0.1',
      description="A meta-package manager to install software from source, or whatever "
                  "the local package manager is. Designed for easy install of source "
                  "trees for the GNU Radio project.",
      url="http://gnuradio.org/pybombs/",
      download_url="https://gitlab.com/NinjaComics/pybombsurl/",
      author="Ravi Sharan",
      author_email="bhagavathula.ravisharan@gmail.com",
      maintainer="Ravi Sharan B A G",
      maintainer_email="bhagavathula.ravisharan@gmail.com",
      license="GPLv3",
      packages=['pybombsurl',
                'pybombsurl.qt'],
      scripts=['pybombsurl.sh','pybombsurl-term','pybombsurl-qt'],
      data_files=[('share/pybombsurl/',
                   ["pybombsurl/qt/pybombsurl-qt.ui"]),
                  ('bin/',
                   ["pybombsurlicons_rc.py"]),
                  ('../etc/firefox/pref/',
                   ["data/pybombsurl.js"]),
                  ('share/kde4/services/',
                   ["data/pybombs.protocol","data/pybombs+http.protocol"]),
                  ('share/applications/',
                   ["data/pybombsurl.desktop"]),
                 ],
      install_requires=deps,
      classifiers=[
        "Development Status :: 3 - Alpha",
        "Environment :: Console",
        "Intended Audience :: System Administrators",
        "Intended Audience :: Telecommunications Industry",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
        "Operating System :: POSIX",
        "Programming Language :: Python :: 3.5",
        "Topic :: Communications :: Ham Radio",
        "Topic :: Software Development :: Build Tools",
        "Topic :: Software Development :: Embedded Systems",
        "Topic :: System :: Archiving :: Packaging",
        "Topic :: System :: Installation/Setup",
        "Topic :: System :: Software Distribution",
        "Topic :: Utilities",
      ]
      )
